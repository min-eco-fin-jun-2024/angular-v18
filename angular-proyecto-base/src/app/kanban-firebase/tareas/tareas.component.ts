import { Component,inject, OnInit } from '@angular/core';
import { ITarea } from '../tarea';
import { FormBuilder, Validators} from '@angular/forms';

import { TareasService } from '../tareas.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-tareas',
  templateUrl: './tareas.component.html',
  styleUrl: './tareas.component.css'
})
export class TareasComponent implements OnInit {

  tareas : ITarea[];
  tarea : ITarea;
  openModal : string;
  private idTarea = 0;

  dragData : any;

  private fb = inject(FormBuilder);
  private tareasService = inject(TareasService);

  accion:string;

  form = this.fb.group({
    id:[''],
    titulo:['',[Validators.required]],
    descripcion :['',Validators.required],
    estado:['backlog',Validators.required]
  });

  constructor(){
    this.tareas = [];
    this.tarea = {id:'',titulo:'',descripcion:'',estado:''};
    this.openModal = 'none';
    this.accion = 'add';
  }

  ngOnInit(): void {
      this.cargarTareas();
  }

  cargarTareas(){
    Swal.fire({
      allowOutsideClick:false,
      text: 'Loading data ...'
    });
    Swal.showLoading();
    this.tareasService.getTareas().subscribe({
      next: resp=>{
        this.tareas = [...resp];
        Swal.close();
      },
      error:ex=>{
        console.log(ex);
        Swal.close();
      }
    });
  }

  onAdd(){
    this.openModal='block';
    this.accion = 'add';
    this.form.reset();
    this.form.patchValue({estado:'backlog'});
  }

  onRegistrarTarea(){
    if(this.form.valid){
      const tar = this.form.value as ITarea;
      if(this.accion == 'add'){
        this.tareasService.addTarea(tar);
      }
      if(this.accion == 'edit'){
        this.tareasService.updateTarea(this.tarea);
      }

      this.form.reset();
      this.openModal = 'none';
      this.form.patchValue({estado:'backlog'});
    }
  }

  editarTarea(tar:ITarea){
    this.form.patchValue(tar);
    this.openModal = 'block';
    this.accion = 'edit';
  }

  eliminarTarea(tar:ITarea){
    //this.tareas = this.tareas.filter(t => t.id !== tar.id);
    this.tareasService.removeTarea(tar);
  }

  //drag and drop
  dragStart(event: DragEvent,tar:ITarea) {
    this.dragData = event.target;
    this.tarea = tar;
  }

  allowDrop(event: DragEvent) {
    event.preventDefault();
  }

  drop(event: DragEvent,estado:string) {
    event.preventDefault();
    const target = event.target as HTMLElement;
    target.appendChild(this.dragData);
    // update tarea
    this.tarea.estado = estado;
    this.tareasService.updateTarea(this.tarea);
  }






}
