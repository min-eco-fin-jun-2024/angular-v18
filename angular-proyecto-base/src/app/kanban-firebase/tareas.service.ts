import { Injectable,inject } from '@angular/core';
import { ITarea } from './tarea';
import { Firestore,collection,addDoc, collectionData,deleteDoc,setDoc,doc }
 from '@angular/fire/firestore'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TareasService {

  private firestore = inject(Firestore);

  constructor() { }

  addTarea(tarea:ITarea){
    const tarearef = collection(this.firestore,'tareas');
    addDoc(tarearef,tarea)
  }

  getTareas():Observable<ITarea[]>{
    const tarearef = collection(this.firestore,'tareas');
    return collectionData(tarearef,{idField:'id'}) as Observable<ITarea[]>
  }

  removeTarea(tarea:ITarea){
    const tarearef = doc(this.firestore,`tareas/${tarea.id}`);
    return deleteDoc(tarearef);
  }

  updateTarea(tarea:ITarea){
    const tarearef = doc(this.firestore,`tareas/${tarea.id}`);
    return setDoc(tarearef,tarea);
  }

}
