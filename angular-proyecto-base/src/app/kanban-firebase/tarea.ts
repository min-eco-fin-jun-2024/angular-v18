export interface ITarea {
    id?:string; // ? propiedad opcional
    titulo:string;
    descripcion:string;
    estado:string;
}
