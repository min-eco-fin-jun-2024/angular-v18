import { Injectable } from '@angular/core';

import { Product } from './product';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StoreService {

  //store
  private myProducts : Product[];
  // definicion de myCart como operaciones asíncronas y eventos : BehaviorSubject
  private myCart = new BehaviorSubject<Product[]>([]);

  // definimos el observable
  myCart$ = this.myCart.asObservable();

  constructor() { 
    this.myProducts = [];
  }

  addProduct(product:Product){
    //adicionamos al store
    this.myProducts.push(product);
    //emita el ultimo valor del store
    this.myCart.next(this.myProducts);
  }

}
