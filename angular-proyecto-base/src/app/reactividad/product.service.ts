import { Injectable, inject } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {Product,ProductList } from './product';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  URL_API: string = 'https://dummyjson.com/products';
  
  private http = inject(HttpClient);

  constructor() { }

  getProducts(){
    return this.http.get(this.URL_API)
  }

}
