import { Component,inject } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StoreService } from '../store.service';
import { CartListComponent } from '../cart-list/cart-list.component';

@Component({
  selector: 'app-nav-bar-cart',
  standalone: true,
  imports: [CommonModule,CartListComponent],
  templateUrl: './nav-bar-cart.component.html',
  styleUrl: './nav-bar-cart.component.css'
})
export class NavBarCartComponent {

    private storeService = inject(StoreService);
    displaySidebar = 'none';

    // convencion para indicar un observable
    myCart$ = this.storeService.myCart$



}
