import { Component,inject } from '@angular/core';

import { StoreService } from '../store.service';
import { CommonModule} from '@angular/common';

@Component({
  selector: 'app-cart-list',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './cart-list.component.html',
  styleUrl: './cart-list.component.css'
})
export class CartListComponent {

  private storeService = inject(StoreService);
  // convencion para indicar un observable
  myCart$ = this.storeService.myCart$

}
