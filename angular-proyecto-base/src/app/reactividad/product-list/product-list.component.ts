import { Component,inject } from '@angular/core';

import { ProductService } from '../product.service';
import { StoreService } from '../store.service';
import { ProductList,Product } from '../product';

import {NavBarCartComponent} from '../nav-bar-cart/nav-bar-cart.component';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-product-list',
  standalone: true,
  imports: [NavBarCartComponent],
  templateUrl: './product-list.component.html',
  styleUrl: './product-list.component.css'
})
export class ProductListComponent {

  productList : ProductList;
  products : Product[];

  private productService = inject(ProductService);
  private storeService = inject(StoreService);

  constructor(){
    this.productList = {products :[] ,total:0,skip:0,limit:0};
    this.products = [];
  }

  ngOnInit(): void {
    this.loadProducts();
  }

  loadProducts(){

    Swal.fire({
      allowOutsideClick:false,
      text: 'Loading data ...'
    });
    Swal.showLoading();
    this.productService.getProducts().subscribe({
      next: resp=>{
        this.productList = resp as ProductList;
        this.products = this.productList.products;
        Swal.close();
      },
      error: ex=>{
        console.log(ex);
        Swal.close();
      }
    })

  }

  addProductToCart(product: Product){
    this.storeService.addProduct(product);
  }


}
