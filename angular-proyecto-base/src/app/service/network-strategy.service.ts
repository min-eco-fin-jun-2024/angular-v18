import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable,of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NetworkStrategyService implements PreloadingStrategy {

  constructor() { }

  preload(route: Route, fn: () => Observable<any>): Observable<any> {
    
    if(this.hasGoodConnection()){
      console.log('Good connection, preloading:', route.path);
      return fn();
    } else {
      console.log('Poor connection, not preloading:', route.path);
      return of(null)
    }

  }

  private hasGoodConnection(): boolean {

    if('connection' in navigator){
      const conn = (navigator as any).connection;

      if(conn.saveData){
        return false; // No precargar 
      }

      const effectiveType = conn.effectiveType;
      console.log(effectiveType);
      const goodConnections = ['4g','3g'];

      return goodConnections.includes(effectiveType); // true precarga, false no precarga
    }
    return true;
  }

}
