import { TestBed } from '@angular/core/testing';

import { NetworkStrategyService } from './network-strategy.service';

describe('NetworkStrategyService', () => {
  let service: NetworkStrategyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NetworkStrategyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
