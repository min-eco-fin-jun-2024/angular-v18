import { ApplicationConfig, provideZoneChangeDetection, isDevMode } from '@angular/core';
import { NoPreloading, PreloadAllModules, provideRouter, withPreloading } from '@angular/router';

import { routes } from './app.routes';

import { provideHttpClient } from '@angular/common/http';
import { provideStore } from '@ngrx/store';
import { counterReducer } from './my-counter/counter-reducer';
import { shopReducer } from './shopping-cart-redux/store/shop-reducer';
import { initializeApp, provideFirebaseApp } from '@angular/fire/app';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';

import { environment } from '../environments/environment';
import { provideServiceWorker } from '@angular/service-worker';

import { NetworkStrategyService } from './service/network-strategy.service';

export const appConfig: ApplicationConfig = {
  providers: [
    provideZoneChangeDetection({ eventCoalescing: true }),
    provideRouter(routes,withPreloading(NetworkStrategyService)),
    provideHttpClient(),
    provideStore({ counter: counterReducer, 'shop': shopReducer }),
    provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    provideServiceWorker('ngsw-worker.js', {
      enabled: environment.production,
      registrationStrategy: 'registerWhenStable:30000'
    })
  ]
};
