import { Component,inject,signal } from '@angular/core';
import { CommonModule } from '@angular/common'
import {FormBuilder,FormGroup,Validators,ReactiveFormsModule} from '@angular/forms';

interface Task{
  id:number;
  text: string;
  completed:boolean;
}

@Component({
  selector: 'app-task-signals',
  standalone: true,
  imports: [CommonModule,ReactiveFormsModule],
  templateUrl: './task-signals.component.html',
  styleUrl: './task-signals.component.css'
})
export class TaskSignalsComponent {

  taskForm : FormGroup;

  private fb = inject(FormBuilder);

  //tasks : Task[] = [{id:1,text: 'task 1',completed:true}];
  tasks = signal<Task[]>([{id:1,text: 'task 1',completed:true}]);

  openModal : string = 'none';

  isEdit = signal<boolean>(false);

  // Reactive Forms
  // 1.FormControl
  // 2.FormGroup
  // 3.FormArray
  // 4.FormBuilder
  // 5.Validators
  constructor(){
    this.taskForm = this.fb.group({
      id : [0,Validators.required],
      text: ['',[Validators.required, Validators.minLength(3)] ],
      completed:[false]
    });
    
  }

  onSubmit(){

    if(this.taskForm.valid){
      let task :Task = this.taskForm.value;
      //update task
      if(this.isEdit()){
        this.tasks.update(els=>els.map(tk => tk.id === task.id ? {...tk,...task} : tk  ))
  
      }else{
        // add task
        task.id = this.tasks().length + 1;
        this.tasks.update(els=>[...els,task]);
      }
      this.taskForm.reset();
      this.openModal = 'none';
    }else{
      console.log('Form is invalid!');
    }
    
  }

  editTask(task:Task){
    this.taskForm.setValue(task);
    this.openModal = 'block';
    this.isEdit.set(true);
  }

  removeTask(id:number){
    this.tasks.update(els=>els.filter(itm=> itm.id !==id));
  }



}
