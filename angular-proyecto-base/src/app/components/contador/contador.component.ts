import { ChangeDetectorRef, Component,inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { interval } from 'rxjs';

@Component({
  selector: 'app-contador',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './contador.component.html',
  styleUrl: './contador.component.css'
})
export class ContadorComponent {

  counter = 0;
  counterInterval = 0;
  counterObs$ = this.getCounter();

  constructor(){
    setInterval(()=>{
      this.counterInterval++;
    },1000);
  }

  increment():void{
    this.counter++;
  }

  getCounter() {
    return interval(1000);
  }

}
